# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


insurances = Insurance.create ([{ name: 'Redmoon' , street_address: '283 blair Avenue'},
                                { name: 'Moonshine' , street_address: '1350 river oaks'},
                                { name: 'HMl' , street_address: '2965 sam Way'},
                                { name: 'HMO' , street_address: '5560 calhoun Street'},
                                { name: 'lagnfield insurance' , street_address: '321 langfield Street'}])

specialists = Specialist.create([{ name: 'jimmy johns', specialty:'fast food' },
                                 { name: 'lilo smith', specialty:'driver' },
                                 { name: 'andrea lane', specialty:'secretary' },
                                 { name: 'andres jordan', specialty:'sports' },
                                 { name: 'xing ding', specialty:'teach' }])

patients = Patient.create([{ name: 'jammie fox', street_address: '356 lame rd', :insurance_id => 2},
                           { name: 'paul walker', street_address: '9968 john Street', :insurance_id => 6},
                           { name: 'xzivit', street_address: '555 nasa prkw', :insurance_id => 8 },
                           { name: 'thor greg', street_address: '897 lone road', :insurance_id => 7 },
                           { name: 'billy many', street_address: '23500 capacity ln', :insurance_id => 5}])


 Appointment.create( :specialist_id =>1, :patient_id=>1, complaint: 'fractured rib', appointment_date: '2014-12-12 ', fee: '5600.99')
  Appointment.create( :specialist_id =>2, :patient_id => 6, complaint: 'stomach ache', appointment_date: '2014-10-3 ', fee: '89.67')
  Appointment.create( :specialist_id =>3, :patient_id => 2, complaint: 'overdose', appointment_date: '2014-11-30 ', fee: '156.90')
  Appointment.create( :specialist_id =>4, :patient_id => 7, complaint: 'athletes foot', appointment_date: '2014-12-21 ', fee: '1000.00')
  Appointment.create( :specialist_id =>8, :patient_id => 3, complaint: 'broken nails', appointment_date: '2014-11-17 ', fee: '512.45')
  Appointment.create( :specialist_id =>7, :patient_id => 8, complaint: 'internal bleeding', appointment_date: '2014-10-25 ', fee: '145.59')
  Appointment.create( :specialist_id =>6, :patient_id => 3, complaint: 'open sword wound', appointment_date: '2014-10-30 ', fee: '165.00')
  Appointment.create( :specialist_id =>5, :patient_id => 9, complaint: 'blury vision', appointment_date: '2014-11-05 ', fee: '1112.80')
  Appointment.create( :specialist_id =>2, :patient_id => 4, complaint: 'mri', appointment_date: '2014-11-18 ', fee: '340.34')
  Appointment.create( :specialist_id =>1, :patient_id => 9, complaint: 'CAT scan ', appointment_date: '2014-12-24 ', fee: '735.00')
  Appointment.create( :specialist_id =>1, :patient_id => 5, complaint: 'tuberculosis', appointment_date: '2014-10-29 ', fee: '163.45')